import java.io.IOException;

/**
 * the main class file for controling the sort
 * 
 * @author syedfarhan
 * @author hmegan
 * @version November
 */
public class HeapSortControl {
    /**
     * the parser to parse each block
     */
    private Reader p;

    /**
     * size, in records of a block
     */
    public static final int BLOCKSIZE = 512;

    /**
     * heap for sorting
     */
    private Minheap<Record> heap;

    /**
     * heap storage
     */
    protected Record[] records;

    /**
     * size of the record array (heap storage)
     */
    private int recordSize;

    /**
     * the input buffer
     */

    protected Record[] inputBuffer;

    /**
     * iterator for the input buffer
     */
    private int inputBufferPos;

    /**
     * output buffer
     */
    protected Record[] outputBuffer;
    /**
     * iterator for the output buffer
     */
    private int outputBufferPos;

    /**
     * array to track the start of runs
     */
    protected int[] runStarts;
    /**
     * total number of runs
     */
    protected int numRuns;

    /**
     * size of the file we're working wiht
     */
// private int fileSize;

    /**
     * writer to write the runs file to disk
     */
    protected Writer w;

    /**
     * tells if we need to merge
     */
    protected boolean needMerging;

    /**
     * Constructor
     * 
     * @param p1
     *            the reader for the input file
     * @throws IOException
     *             when trouble reading the file
     */
    public HeapSortControl(Reader p1) throws IOException {
        p = p1;

        // initializing arrays
        inputBuffer = new Record[BLOCKSIZE];
        records = new Record[BLOCKSIZE * 8];
        outputBuffer = new Record[BLOCKSIZE];

        // reading first block and creating the minheap
        for (int i = 0; i < BLOCKSIZE * 8; i++) {
            records[i] = p.readNextRecord();
        }
        recordSize = records.length;
        heap = new Minheap<Record>(records, records.length, records.length);

        // reading input buffer
        inputBufferPos = 0;
        this.readInputblock();

        // setting up run counting mechanism
        int fileSize;
        fileSize = p.getFileSize();
        int maxNumRuns = fileSize / (records.length * 16);
        needMerging = (maxNumRuns > 1);
        runStarts = new int[maxNumRuns + 1];
        numRuns = 0;
        runStarts[0] = 0;

        // misc
        outputBufferPos = 0;
        if (needMerging) {
            w = new Writer("RunsFile.bin");
        }
        else {
            w = new Writer("out_" + p.getName());
        }
    }


    /**
     * preps next run
     * 
     * @return true if heap can add to the current run
     */
    public boolean prepNextRun() {
        if (recordSize == 0) {
            return false;
        }
        else if (recordSize == records.length) {
            heap = new Minheap<Record>(records, recordSize, recordSize);
            return true;
        }
        else {
            // copy over everything to the statr of the array
            for (int i = 0; i < recordSize; i++) {
                records[i] = records[i + records.length - recordSize];
            }
            heap = new Minheap<Record>(records, recordSize, records.length);
            return true;
        }
    }


    /**
     * Creates the next run and writes to the output file/buffer
     * There may be some items that are not written to the output file
     * and remain the the buffer
     * Make sure heap is not empty before calling
     * 
     * @throws IOException
     *             if problems reading input file
     */
    public void createNextRun() throws IOException {
        /*
         * records = new Record[BLOCKSIZE];
         * for (int i = 0; i < BLOCKSIZE; i++ ) {
         * try {
         * records[i] = p.readNextRecord();
         * }
         * catch (IOException e) {
         * 
         * e.printStackTrace();
         * }
         * }
         * 
         * Minheap<Record> thisHeap = new Minheap<Record>(records, BLOCKSIZE,
         * BLOCKSIZE);
         */
        // TODO printouts
        // TODO track the position of runs
        // TODO update for last run
        int runLength = 0;
        while (heap.heapsize() > 0) {
            // TODO print outs
            Record next = records[0];
            // System.out.println(next.getKey());
            // copy lowest value of heap into outputbuffer
            outputBuffer[outputBufferPos] = next;
            outputBufferPos++;
            runLength++;
            if (outputBufferPos >= outputBuffer.length) {
                this.writeOutputBufferToFile(!needMerging);
            }

            if (inputBuffer[inputBufferPos] != null) { // if there is some input
                // check if next input can come with this heap
                if (inputBuffer[inputBufferPos].compareTo(next) < 0) {
                    heap.removemin();
                    records[heap.heapsize()] = inputBuffer[inputBufferPos];
                    // add to the last position of the heap and decrement heap
                    // size
                }
                else {
                    records[0] = inputBuffer[inputBufferPos];
                    heap.siftdown(0);
                }

                inputBufferPos++;
                if (inputBufferPos == inputBuffer.length) {
                    this.readInputblock();
                }
            }
            else {
                heap.removemin();
                recordSize--;
            }
        }

        runStarts[numRuns + 1] = runStarts[numRuns] + runLength;
        numRuns++;

        // add next value from input and heapify
        // check if next input is compatible with this heap
    }


    /**
     * reads in a new block to the input buffer
     * 
     * @return false if reached the end of file
     * @throws IOException
     *             if IOexception
     */
    private boolean readInputblock() throws IOException {
        Record curr;
        for (int i = 0; i < BLOCKSIZE; i++) {
            curr = p.readNextRecord();
            if (curr == null) {
                inputBuffer[i] = curr;
                inputBufferPos = 0;
                return false;
            }
            inputBuffer[i] = curr;
            // System.out.println(curr.getKey());
        }
        inputBufferPos = 0;
        return true;
    }


    /**
     * writes the current output buffer to the file
     * 
     * @param print
     *            tells the writer if this is teh final go and thus a print
     *            needs to be executed
     * @throws IOException
     */
    private void writeOutputBufferToFile(boolean print) throws IOException {
        w.write(outputBuffer, outputBufferPos, print);
        outputBufferPos = 0;
    }

}
