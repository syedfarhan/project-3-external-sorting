import java.io.FileNotFoundException;
import java.io.IOException;
import student.TestCase;

/**
 * testing the sort control
 * 
 * @author syedfarhan
 * @author hmegan
 * @version November
 */
public class HeapSortControlTest extends TestCase {
    /**
     * will test multiple runs
     * @throws IOException 
     */

    public void testMultipleRuns() throws IOException {
        Reader p;
        p = new Reader("Test2.bin");
  
        HeapSortControl s;

        s = new HeapSortControl(p);
        do {
            s.createNextRun();
        }
        while (s.prepNextRun());

        p = new Reader("RunsFile.bin");

        int[] runs = s.runStarts;
        int numRuns = s.numRuns;
        int runStart;
        int runEnd;
        double lastRecordKey;
        for (int run = 0; run < numRuns; run++) {
            runStart = runs[run];
            runEnd = runs[run + 1];
            lastRecordKey = 0;
            for (int i = runStart; i < runEnd; i++) {
                Record r = p.readNextRecord();
                assertTrue(r.getKey() > lastRecordKey);
                lastRecordKey = r.getKey();
            }
        }

        

    }

}
