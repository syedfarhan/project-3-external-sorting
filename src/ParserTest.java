import java.io.IOException;
import student.TestCase;

/**
 * Testing the parser
 * 
 * @author Syed Muhammad Farhan (syedfarhan)
 * @author Megan Hicks (Hmegan)
 * @version 09/10/2020
 */

public class ParserTest extends TestCase {
    /**
     * testing the parsing initialization code
     * 
     * @throws IOException
     *             when something bad happens
     */
    public void testManual() throws IOException {
        Reader p = new Reader("RandomInput");
        assertNotNull(p);
        Record r;
        r = p.readNextRecord();
        assertEquals(0.35893657871808626, r.getKey(), 0.0000001);

        r = p.readNextRecord();
        for (int i = 0; i < 511 * 2; i++) {
            System.out.println(r.getKey());
            r = p.readNextRecord();
        }
        assertEquals(0.27837519924547904, r.getKey(), 0.0000001);

        r = p.readNextRecord();
        assertNull(r);
    }
}
