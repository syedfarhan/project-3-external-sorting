import java.io.FileNotFoundException;
import java.io.IOException;
import student.TestCase;

/**
 * @author hmegan
 * @author syedfarhan
 * @version October
 */
public class ExternalsortTest extends TestCase {

    /**
     * set up for tests
     */
    public void setUp() {
        // nothing to set up.
    }


    /**
     * Get code coverage of the class declaration.
     */
    public void testExternalsortInit() {
        Externalsort sorter = new Externalsort();
        assertNotNull(sorter);
        Externalsort.main(new String[2]);
        assertTrue(systemOut().getHistory().contains("incorrect"));
    }


    /**
     * This will test one tun for the sort
     * @throws IOException if problem reading files
     */
    public void testOneRun() throws IOException {
        String[] args = new String[1];
        args[0] = "Test1";
        Externalsort.main(args);

        Reader r = new Reader("out_Test1");
        Record lastRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        Record thisRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        int counter = 0;
        while (thisRecord != null && counter < 50) {
            counter++;
            assertTrue(lastRecord.compareTo(thisRecord) <= 0);
            lastRecord = thisRecord;
            thisRecord = r.readNextRecord();

        }

    }


    /**
     * testing multiple merges
     * 
     * @throws IOException
     *             if IO problem
     */

    public void testMultipleRuns() throws IOException {
        String[] args = new String[1];
        args[0] = "mergetest.bin";
        Externalsort.main(args);

        Reader r = new Reader("out_mergetest.bin");
        Record lastRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        Record thisRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        int counter = 0;
        while (thisRecord != null ) {
            counter++;
            assertFalse(lastRecord.compareTo(thisRecord) > 0);
            lastRecord = thisRecord;
            thisRecord = r.readNextRecord();

        }

    }

    /**
     * testing more than 8 runs
     * @throws IOException if problem reading files
     */
    public void testGreaterThan8Runs() throws IOException {
        String[] args = new String[1];
        args[0] = "mergetest2.bin";
        Externalsort.main(args);

        Reader r = new Reader("out_mergetest2.bin");
        Record lastRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        Record thisRecord = r.readNextRecord();
        // System.out.println(lastRecord.toString());
        int counter = 0;
        while (thisRecord != null ) {
            counter++;
            assertTrue(lastRecord.compareTo(thisRecord) <= 0);
            lastRecord = thisRecord;
            thisRecord = r.readNextRecord();

        }

    }

}
