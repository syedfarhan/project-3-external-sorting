import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * 
 * @author Hmegan
 * @author Syedfarhan
 * @version October
 *
 *          InputOutputManager manages files being read for
 *          the external sort process
 */
public class Writer {

    /**
     * file to write
     */
    private RandomAccessFile runs;

    /**
     * 
     */
    private int numPrints;

    /**
     * constructor for the writer
     * 
     * @param fileSize
     *            the size of the file, in bytes
     * @throws IOException
     *             when trouble creating the file
     */
    Writer(String fileName) throws IOException {
        numPrints = 0;
        runs = new RandomAccessFile(fileName, "rw");
        runs.seek(0);

    }


    /**
     * write outbuffer to file
     * 
     * @throws IOException
     * @param buffer
     *            is the output buffer
     * @param l
     *            is the number of records to write from the buffer
     * @param print tells if we need to print the first record
     */
    public void write(Record[] buffer, int l, boolean print)
        throws IOException {
        if (print) {
            if (numPrints % 5 != 0) {
                System.out.print(" ");
            }
            buffer[0].printRecord();
            numPrints++;
            if (numPrints % 5 == 0) {
                System.out.println();
            }
        }
        for (int i = 0; i < l; i++) {
            Record r = buffer[i];
            // System.out.println(r.getKey());
            byte[] toWrite = r.getCompleteRecord();
            runs.write(toWrite);
        }
    }
    
    public void close() throws IOException {
        runs.close();
    }


}
