import java.io.IOException;

public class MultiWayMerger {
    /**
     * array where data will be stored. This should be the same as the heap
     * array
     */
    private Record[] data;
    /**
     * buffer for reading from input
     */
    private Record[] inputBuffer;
    /**
     * Buffer before writing to output
     */
    private Record[] outputBuffer;

    private int outBufferIterator;

    private Reader r;

    // private int[] recordStarts;
    // private int[] recordEnds;
    // private int numRuns;
    /**
     * Constructor, just setting up memory and input/output buffers
     * 
     * @param mem
     *            is the space to be used
     * @param inBuffer
     *            is the space for the input buffer
     * @param outBuffer
     *            is the space for the output buffer
     * @param reader
     *            is the reader for the runsfile (no need to seek)
     */
    public MultiWayMerger(
        Record[] mem,
        Record[] inBuffer,
        Record[] outBuffer,
        Reader r1) {
        data = mem;
        inputBuffer = inBuffer;
        outputBuffer = outBuffer;
        outBufferIterator = 0;
        r = r1;
    }


    /**
     * Merges upto the memorysize/8 blocks into a single run. writer will be
     * updated to the end of the current run
     * 
     * @param numRuns
     *            is the number of runs to be merged
     * @param runStarts
     *            is an array with the start of the runs
     * @param runEnds
     *            is an array with the end of the runs (valie not incuded)
     * @param w
     *            the writer to use to write to file. Please ensure you seek to
     *            a position where the output does not override uesful data
     * @param p
     *            tells if we need to output (should be true for the final merge
     *            call)
     * @return the size of the new run
     * @throws IOException
     */
    public int merge(
        int numRuns,
        int[] runStarts,
        int[] runEnds,
        Writer w,
        boolean p)
        throws IOException {
        int runSize = 0; // TODO think about why this is here
        int[] iterators = new int[numRuns];
        boolean[] activeRuns = new boolean[numRuns];
        int[] numItemsHandled = new int[numRuns];
        for (int i = 0; i < numRuns; i++) {
            readInputToMemory(runStarts[i], 512, i * 512);
            iterators[i] = i * 512;
            activeRuns[i] = true;
            numItemsHandled[i] = 0;
        }

        int s;
        Record smallest;
        int numRunsMax = numRuns;
        while (numRuns > 0) {

            // find the run that has teh smallest value
            s = -1;
            for (int i = 0; i < numRunsMax; i++) {
                if (activeRuns[i]) {
                    if (s == -1) {
                        s = i;
                    }
                    else if (data[iterators[i]].compareTo(
                        data[iterators[s]]) <= 0) {
                        s = i;
                    }
                }
            }
            smallest = data[iterators[s]];

            // write output buffer to file if full
            outputBuffer[outBufferIterator] = smallest;
            outBufferIterator++;
            if (outBufferIterator >= outputBuffer.length) {
                this.writeOutputToFile(w, p);
            }

            iterators[s]++;
            numItemsHandled[s]++;
            runSize++;

            // check if we've exhausted the sth run
            if (runStarts[s] + numItemsHandled[s] >= runEnds[s]) {
                numRuns--;
                activeRuns[s] = false;
                continue;
            }

            // check if we've exhausted the sth data in memory
            if (iterators[s] % 512 == 0) {
                // if we have more than 512 records remaining in the sth run
                if (runStarts[s] + numItemsHandled[s] + 512 < runEnds[s]) {
                    this.readInputToMemory(runStarts[s] + numItemsHandled[s],
                        512, iterators[s] - 512);
                }
                // if we have less than 512 records remaining in sth run
                else {
                    this.readInputToMemory(runStarts[s] + numItemsHandled[s],
                        runEnds[s] - (runStarts[s] + numItemsHandled[s]),
                        iterators[s] - 512);
                }
                iterators[s] -= 512;
            }
        }
        if (outBufferIterator>0) {
            this.writeOutputToFile(w, p);
        }

        return runSize;

    }


    private boolean readInputToMemory(
        int startInfile,
        int length,
        int startInMemory)
        throws IOException {
        Record curr;
        r.seek(startInfile);
        for (int i = 0; i < length; i++) {
            curr = r.readNextRecord();
            inputBuffer[i] = curr;
            data[startInMemory + i] = curr;
            // System.out.println(curr.getKey());
        }
        return true;
    }


    private void writeOutputToFile(Writer w, boolean print) throws IOException {
        w.write(outputBuffer, outBufferIterator, print);
        outBufferIterator = 0;
    }

}
