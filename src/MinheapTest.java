import student.TestCase;

// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.

/**
 * MinheapTest tests all the methods of Minheap
 * up to date again
 * 
 * @author Hmegan
 * @author Syedfarhan
 * @version October
 */
public class MinheapTest extends TestCase {

    private Minheap<Integer> m;
    @SuppressWarnings("rawtypes")
    private Comparable[] test;

    /**
     * setUp creates a new Minheap and test array
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void setUp() {
        test = new Comparable[15];
        m = new Minheap(test, 0, 5);
    }


    /**
     * testInsert tests insert method
     * also tests isFull as subsidiary function of insert
     */
    public void testInsert() {
        assertEquals(0, m.heapsize());
        m.insert(88);
        assertEquals(1, m.heapsize());
        m.insert(79);
        m.insert(80);
        m.insert(81);
        m.insert(82);

        assertTrue(m.isFull());
        m.insert(45);

    }


    /**
     * testIsLeaf tests Leaf method
     */
    public void testIsLeaf() {
        m.insert(88);
        m.insert(83);
        assertFalse(m.isLeaf(0));
        assertTrue(m.isLeaf(1));
        assertFalse(m.isLeaf(3));

    }


    /**
     * testBranches tests leftchild, rightchild,
     * and parent methods
     */
    public void testBranches() {
        m.insert(68);
        m.insert(83);
        m.insert(79);
        assertEquals(1, m.leftchild(0));
        assertEquals(2, m.rightchild(0));
        assertEquals(-1, m.leftchild(3));
        assertEquals(-1, m.rightchild(3));

        assertEquals(-1, m.parent(0));
        assertEquals(0, m.parent(1));
        assertEquals(0, m.parent(2));
        assertEquals(-1, m.parent(3));

    }


    /**
     * testRemovemin tests Removemin method
     */
    public void testRemovemin() {
        assertEquals(-1, m.removemin());
        m.insert(68);
        m.insert(83);
        m.insert(79);
        assertEquals(68, m.removemin());
    }


    /**
     * testRemove tests Remove method
     */
    public void testRemove() {
        assertEquals(-1, m.remove(0));
        assertEquals(-1, m.remove(1));
        m.insert(68);
        m.insert(83);
        m.insert(65);

        assertEquals(-1, m.remove(-1));
        assertEquals(-1, m.remove(3));
        assertEquals(83, m.remove(1));
        assertEquals(65, m.remove(0));
        m.insert(25);
        assertEquals(25, m.remove(0));

    }


    /**
     * testModify tests Modify method
     */
    public void testModify() {
        m.insert(68);
        m.insert(83);
        m.insert(65);

        assertEquals(68, (int)m.getPos(2));
        m.modify(2, 25);
        assertEquals(25, (int)m.getPos(0));
    }


    /**
     * testBigHeap tests that all values are managed correctly
     * when the heap has height = 3
     */
    public void testBigHeap() {

        Minheap<Integer> big;
        big = new Minheap(test, 0, 15);
        big.insert(68);
        big.insert(83);
        big.insert(65);
        big.insert(45);
        big.insert(53);
        big.insert(22);
        big.insert(69);
        big.insert(71);
        big.insert(72);
        big.insert(74);
        big.insert(33);
        assertEquals(22, (int)big.getPos(0));
        assertEquals(33, (int)big.getPos(1));
        assertEquals(45, (int)big.getPos(2));
        assertEquals(71, (int)big.getPos(3));
        assertEquals(53, (int)big.getPos(4));
        assertEquals(68, (int)big.getPos(5));
        assertEquals(69, (int)big.getPos(6));
        assertEquals(83, (int)big.getPos(7));
        assertEquals(72, (int)big.getPos(8));
        assertEquals(74, (int)big.getPos(9));
        assertEquals(65, (int)big.getPos(10));
        assertNull(big.getPos(11));
        assertEquals(65, big.remove(10));

        assertEquals(22, (int)big.removemin());
        assertEquals(53, (int)big.getPos(1));
        assertEquals(71, (int)big.remove(3));
        assertEquals(72, (int)big.getPos(3));
        big.buildheap();

    }

}
