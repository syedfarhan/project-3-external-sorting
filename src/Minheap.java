// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.

/**
 * Minheap manages the data for external sort
 * up to date again
 * 
 * @author Hmegan
 * @author Syedfarhan
 * @version October
 * @param <E>
 *            the type of data you want to store in the heap
 */
public class Minheap<E extends Comparable<? super E>> {

    /**
     * the array used for the heap
     */
    private E[] heap; // Pointer to the heap array
    /**
     * the maximum size of the heap
     */
    private int size; // Maximum size of the heap
    /**
     * the current size of teh heap
     */
    private int n; // Number of things now in heap

    /**
     * flag for when the heap is full
     */
    //private Boolean full = false;

    // Constructor supporting preloading of heap contents
    /**
     * Minheap constructor sets up the minheap
     * 
     * @param h
     *            - pointer to the heap array
     * @param num
     *            - number of items in heap
     * @param maxSize
     *            - max size of heap
     */
    public Minheap(E[] h, int num, int maxSize) {
        heap = h;
        n = num;
        size = maxSize;
        buildheap();
    }


    /**
     * heapsize returns the current size of the heap
     * 
     * @return - n = size of heap
     */
    int heapsize() {
        return n;
    }


    /**
     * isLeaf tells if a pos is a leaf or not
     * 
     * @param pos
     *            - position in heap
     * @return true if leaf, false if not
     */
    boolean isLeaf(int pos) {
        return (pos >= n / 2) && (pos < n);
    }


    /**
     * leftchild returns the position of
     * leftchild of current position
     * 
     * @param pos
     *            - position in question
     * @return - position of left child
     */
    int leftchild(int pos) {
        if (pos >= n / 2) {
            return -1;
        }
        return 2 * pos + 1;
    }


    /**
     * rightchild returns the position of
     * rightchild of current position
     * 
     * @param pos
     *            - position in question
     * @return - position of right child
     */
    int rightchild(int pos) {
        if (pos >= (n - 1) / 2) {
            return -1;
        }
        return 2 * pos + 2;
    }


    /**
     * parent returns the position of
     * parent of current position
     * 
     * @param pos
     *            - position in question
     * @return - position of parent
     */

    int parent(int pos) {
        if ((pos <= 0) || (pos >= n)) {
            return -1;
        }
        return (pos - 1) / 2;
    }


    /**
     * Inserts val into heap
     * 
     * @param key
     *            - the value to be inserted
     */
    void insert(E key) {
        if (isFull()) {
            return;
        }
        int curr = n++;
        heap[curr] = key; // Start at end of heap
        // Now sift up until curr's parent's key < curr's key
        while ((curr != 0) && (heap[parent(curr)].compareTo(heap[curr]) > 0)) {
            swap(heap, curr, parent(curr));
            curr = parent(curr);
        }
    }


    /**
     * isFull returns if the heap is full
     * 
     * @return true if full, false if not
     */
    public Boolean isFull() {
        if (n >= size) {
            System.out.println("heap is Full");
            return true;
        }
        return false;
    }


    /**
     * Heapify contents of heap
     */
    void buildheap() {
        if (n <= 2) {
            return;
        }
        for (int i = n / 2 - 1; i >= 0; i--) {
            siftdown(i);
        }
    }


    /**
     * Put element in its correct place
     * 
     * @param pos
     *            starting point from which to siftdown
     */
    void siftdown(int pos) {
        if ((pos < 0) || (pos >= n)) {
            return; // Illegal position
        }
        while (!isLeaf(pos)) {
            int j = leftchild(pos);
            if ((j < (n - 1)) && (heap[j].compareTo(heap[j + 1]) > 0)) {
                j++; // j is now index of child with lesser value
            }
            if (heap[pos].compareTo(heap[j]) <= 0) {
                return;
            }
            swap(heap, pos, j);
            pos = j; // Move down
        }
    }


    //
    /**
     * Remove and return min value
     * 
     * @return the min value in the heap
     */
    Comparable removemin() {
        if (n == 0) {
            return -1; // Removing from empty heap
        }
        swap(heap, 0, --n); // Swap min with last value
        siftdown(0); // Put new heap root val in correct place
        return heap[n];
    }


    /**
     * Remove and return element at specified position
     * 
     * @param pos
     *            to be removed
     * @return the item removed or -1 if illegal heap position
     */
    Comparable remove(int pos) {
        if ((pos < 0) || (pos >= n)) {
            return -1; // Illegal heap position
        }
        if (pos == (n - 1)) {
            n--; // Last element, no work to be done
        }
        else {
            this.swap(heap, pos, --n); // Swap with last value
            update(pos);
        }
        return heap[n];
    }


    /**
     * Modify the value at the given position
     * 
     * @param pos
     *            position where the value will be changed
     * @param newVal
     *            - the new value at pos
     */
    void modify(int pos, E newVal) {
        if ((pos < 0) || (pos >= n)) {
            return; // Illegal heap position
        }
        heap[pos] = newVal;
        update(pos);
    }


    /**
     * getPos returns the value at the position
     * 
     * @param pos
     *            position requested
     * @return the value at the position
     */
    E getPos(int pos) {
        return heap[pos];
    }


    /**
     * The value at pos has been changed, restore the heap property
     * 
     * @param pos
     *            - where to begin the update process
     */
    void update(int pos) {
        // If it is a big value, push it up
        while ((pos > 0) && (heap[parent(pos)].compareTo(heap[pos]) > 0)) {
            this.swap(heap, pos, parent(pos));
            pos = parent(pos);
        }
        siftdown(pos); // If it is little, push down
    }


    /**
     * Swaps positions of two values
     * 
     * @param heap1
     *            - where the values are located
     * @param i
     *            - first value to be swapped
     * @param j
     *            - second value to be swapped
     */
    private void swap(E[] heap1, int i, int j) {
        E temp = heap1[i];
        heap1[i] = heap1[j];
        heap1[j] = temp;
    }
}
