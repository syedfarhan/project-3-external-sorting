import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This is the code for project 3 of CS5040. We will try to implement external
 * sorting
 */

/**
 * The class containing the main method.
 *
 * @author Hmegan
 * @author Syedfarhan
 * @version October
 */

// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.

public class Externalsort {

    /**
     * Size, in bytes of one block
     */
    public static final int BLOCKSIZE = 8192;

    /**
     * The number of blocks we can store in memory for the heap
     */
    public static final int HEAPSIZE = 8;

    /**
     * @param args
     *            Command line parameters
     */
    public static void main(String[] args) {
        // System.out.print("some thing happened");
        if (args.length != 1) {
            System.out.println("Bad input: incorrect arguments");
            return;
        }
        Reader p;
        try {
            p = new Reader(args[0]);
        }
        catch (FileNotFoundException e) {
            System.out.println("Bad input: Input file not found");
            return;
        }
        HeapSortControl s;

        try {
            s = new HeapSortControl(p);
            do {
                s.createNextRun();
            }
            while (s.prepNextRun());
            if (s.needMerging) {
                //setting up for mergeSort
                Reader readRuns = new Reader("RunsFile.bin");
                MultiWayMerger merger = new MultiWayMerger(s.records, s.inputBuffer, s.outputBuffer, readRuns);
                int numRuns = s.numRuns;
                int[] runStarts = s.runStarts;
                int[] runEnds = new int[numRuns];
                for (int i=0;i<numRuns;i++) {
                    runEnds[i] = runStarts[i+1];
                }
                int temp;
                int currStart = runEnds[numRuns - 1];
                
                // merge until we have 8 or less runs
                while (numRuns>8) {
                    
                    temp = merger.merge(8, runStarts, runEnds, s.w, false);
                    runStarts[0] = currStart;
                    runEnds[0] = runStarts[0] + temp;
                    currStart = runEnds[0];
                    numRuns -= 7;
                    for (int i=1 ; i<numRuns ; i++) {
                        runStarts[i] = runStarts[i+7];
                        runEnds[i] = runEnds[i+7];
                    }
                }
                
                // final merging
                Writer finalWriter = new Writer("out_"+args[0]);
                merger.merge(numRuns, runStarts, runEnds, finalWriter, true);

                finalWriter.close();
                s.w.close();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
       
        //System.out.println();

    }

}
