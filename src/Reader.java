import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * This is the parser class for the external sort. It will read 16 bytes at
 * a time and create records using the record class
 * 
 * @author syedfarhan
 * @author hmegan
 * @version October
 */

public class Reader {
    /**
     * the size in bytes of one record
     */
    public static final int RECORDSIZE = 16;

    /**
     * the RandomAccess file pointing to the current position
     */
    private RandomAccessFile in;

    /**
     * the file to be used
     */
    private File f;

    /**
     * the name of the file being read
     */
    private String name;
    
    /**
     * the getter for the name
     * @return the filename
     */
    public String getName() {
        return name;
    }
    /**
     * Constructor for the parser
     * 
     * @param fileName
     *            is the name of the file to read
     * @throws FileNotFoundException
     *             if file not found
     */
    public Reader(String fileName) throws FileNotFoundException {
        name = fileName;
        f = new File(fileName);
        in = new RandomAccessFile(f, "r");
    }
    /*
     * 
     * byte[] buffer = new byte[RECORDSIZE];
     * try {
     * raf.readFully(buffer);
     * }
     * catch (IOException e) {
     * return null;
     * }
     * Record r = new Record(buffer);
     * return r;
     * }
     */


    /**
     * gets thes next record from the file
     * 
     * @return the next record
     * @throws IOException
     *             if problem reading the file
     */

    public Record readNextRecord() throws IOException {
        // potential improvement could be to read a block in one go
        byte[] buffer = new byte[RECORDSIZE];
        if (in.read(buffer) == -1) {
            return null;
        }
        Record r = new Record(buffer);
        // in.skip(RECORDSIZE/8);
        return r;
    }


    /**
     * getter for file size
     * 
     * @return size of the file, in bytes
     */
    public int getFileSize() {
        return (int)f.length();
    }
    public void seek(int startInfile) throws IOException {
        in.seek(startInfile*RECORDSIZE);
        
    }

}
